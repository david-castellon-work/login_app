import path from 'path';

import {
	src ,
	dest ,
	series ,
	parallel ,
	task ,
	watch
} from 'gulp' ;

import source from 'vinyl-source-stream';

import babel from 'gulp-babel';
import dotenv from 'gulp-dotenv';
import rename from 'gulp-rename';
import sourcemaps from 'gulp-sourcemaps';
import newer from 'gulp-newer';

import nodemon from 'gulp-nodemon';

import jest from '@jacobq/gulp-jest';

const SERVER_SCRIPT_SRC	= 'src/**/*.js' ,
      BUILD_FILE = 'build' ;

//////////////////////////////////////////////////
// Code Transpilation Tasks
//////////////////////////////////////////////////

task ( 'transpile_server_code', () =>
	src(SERVER_SCRIPT_SRC)
	.pipe(sourcemaps.init())
	.pipe( babel ( {
		presets: [
			[ '@babel/preset-env', {
				'targets': {'node': '10'},
			} ],
		],
		plugins: [
			[ 'module-resolver' , {
				alias : {
					root: './src/',
					database: './src/database',
					lib: './src/lib',
				} ,
			} ],
			[ 'add-module-exports' ] ,
		],
	}))
	.pipe(sourcemaps.write('./maps/src/', {
		// includeContent: false
		sourceRoot: path.join(__dirname, 'src/')
	}))
	.pipe(dest('build'))
) ;

task ( 'transpile_dotenv', () =>
	src('.env')
	.pipe(dotenv())
	.pipe(rename('env.json'))
	.pipe(dest('build'))
) ;

task ( 'transpile_unit_test_code' , () =>
	src ( 'unit_tests/**/*.js' )
	.pipe ( newer ( BUILD_FILE ) )
	.pipe ( sourcemaps . init ( ) )
	.pipe ( babel ( {
		presets : [
			[ '@babel/preset-env', {
				'targets': {'node': '10'},
			} ],
		],
		ignore : [
			// '../app',
		],
		plugins: [
			[ 'module-resolver' , {
				alias : {
					root: path . join ( __dirname , 'build' ) ,
				} ,
			} ],
			[ 'add-module-exports' ] ,
		],
	} ) )
	.pipe ( sourcemaps . write ( 'maps/unit_tests' , {
		// includeContent : false
		sourceRoot: path.join(__dirname, 'unit_tests')
	} ) )
	.pipe ( dest ( 'build/unit_tests' ) )
) ;

task (
	'transpile_everything' ,
	series (
		parallel(
			'transpile_dotenv',
			'transpile_server_code',
		),
		'transpile_unit_test_code',
	)
) ;

//////////////////////////////////////////////////
// Unit Testing Tasks
//////////////////////////////////////////////////

task ( 'start_unit_tests' , () =>
	src(path.join(BUILD_FILE, 'unit_tests'))
	.pipe (jest({
		"preprocessorIgnorePatterns": [
			"<rootDir>/node_modules/babel-jest",
		],
		"automock": false,

		testPathDirs: [ 'build/unit_tests' ],

		testEnvironment: [ 'node' ],
//		modulePathIgnorePatterns: [ 'ping' ]

//		setupFilesAfterEnv: [ 'unit_tests/setup.test.js' ] ,
	}))
) ;

//////////////////////////////////////////////////
// Autorestart
//////////////////////////////////////////////////

task ( 'autorestart_server', done => {
	let stream = nodemon({
		script: 'build/main.js',
		ext: 'js',
		done: done,
		tasks: [ 'transpile_server_code' ],
		watch: [ 'src/**/*.js' , 'unit_tests/**/*.js' ],
		nodeArgs: [ '--enable-source-maps' ],
	}) ;
	stream.on('restart', (files) => {
		console.log("--------------------------------------------------");
		console.log("Altered Source Files:");
		console.log(files);
		console.log("--------------------------------------------------");
	} )
	.on('crash', () => {
		const RESTART_TIMER = 10;

		console.log("--------------------------------------------------");
		console.error(`Application has crashed will restart in ${RESTART_TIMER}s.`);
		console.log("--------------------------------------------------");

		stream.emit('restart', 10);
	} ) ;
} ) ;

task ( 'autorestart_unit_testing', (done) => {
	watch ( 'unit_tests/**/*.js' , ( ) => {
		series(
			'transpile_unit_test_code' ,
			'start_unit_tests'
		) ;
	} ) ;
	done ( ) ;
} ) ;

task ( 'move_test_profile_pic', () =>
	src('unit_tests/authenticated/doubly/user/blue-block.png')
	.pipe ( dest ( 'build/unit_tests/authenticated/doubly/user/' ) )
) ;

task ( 'default', series('transpile_server_code', 'transpile_dotenv') ) ;
