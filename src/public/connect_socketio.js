(function connect_socket_io(){
	var socket = io.connect('http://localhost:3000')

	var messages = document.getElementById('messages');
	var form = document.getElementById('form');
	var input = document.getElementById('input');

	console.log(form);

	form.addEventListener('', e => {
	} ) ;

	form.addEventListener('submit', e => {
		e.preventDefault();
		if (input.value) {
			socket.emit('chat message', input.value);
			input.value = '';
		}
	} ) ;

	socket.on("connect_error", err => {
		console.log("Socket not able to connect!");
		console.log("Error!");
		console.log(err);
	} ) ;

	socket.on("connect", socket => {
		console.log("Socket successfully connected!");
		socket.on("chat message", msg => {
			let item = document.createElement('li');
			item.textContent = msg;
			messages.appendChild(item);
			window.scrollTo(0, document.body.scrollHeight);
		} ) ;
	} ) ;

})()
