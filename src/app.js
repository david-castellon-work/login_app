import path from 'path';

import express from 'express';
import morgan from 'morgan';
import helmet from 'helmet';
import cors from 'cors';
import expressValidator from 'express-validator';
import fileupload from 'express-fileupload';

import http from 'http';
import socketio from 'socket.io';

import socket_io_configuration from 'root/api/socket_io_configuration';
import api from 'root/api';

const exp = express ( ) ;

const http_server = http.createServer(exp);
const socket_io_server = socketio(http_server);

socket_io_configuration(socket_io_server);

exp . use ( fileupload ( { createParentPath: true } ) ) ;
exp . use ( cors ( ) ) ;
exp . use ( morgan ( 'dev' ) ) ;
exp . use ( helmet ( ) ) ;
exp . use ( express . json ( ) ) ;
exp . use ( express . urlencoded ( { extended : true } ) ) ;
exp . use ( api ) ;
exp . use ( '/static' , express.static ( path.join(__dirname, 'public') ) ) ;


exp . locals . socket_io_server = socket_io_server;

export default { exp , socket_io_server , http_server } ;
