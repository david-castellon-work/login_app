import jwt from 'jsonwebtoken';
import database from 'database';
import { TOKEN_SECRET , SESSION_TIMEOUT } from 'root/env.json'    ;

// Issues a token to the user.
export default async ( req , res , next ) => {
	console.log("Create Session Route");
	console.log('--------------------------------------------------');

	console.log('Checking Username');
	let user;

	console.log("Before req.body checks!");
	console.log(req.body);
	if (req.body.name) {
		user = await database.users.findOne({ name: req.body.name }).exec();
		console.log("req body has name");
	} else if (req.body.email) {
		user = await database.users.findOne({ email: req.body.email }).exec();
		console.log("req body has email");
	} else if (req.body.phone) {
		user = await database.users.findOne({ phone: req.body.phone }).exec();
	} else {
		console.log("req body has no name or email");
		return res . status ( 200 ) . json ( { message : "No user name or email!" } ) ;
	}

	console.log("After req.body checks!");

	if (!user) {
		return res . status ( 200 ) . json ( { message : "User does not exist!" } ) ;
	}

	console.log("user");
	console.log(user);

	console.log('--------------------------------------------------');

	console.log("Checking Password!");
	if ( !req.body.password ) {
		return res . status ( 200 ) . json ( { message : "No user password!" } ) ;
	}

	const is_password = await user.is_password(req.body.password) ;
	console.log(`is_password: ${is_password}`);
	if ( !is_password ) return res . status ( 200 ) . json ( { message : "Password Mismatch!" } ) ;
	console.log("Passwords Matched!");
	console.log('--------------------------------------------------');

	user.push_session(req.body.session_name);
	console.log(user);

	console.log('--------------------------------------------------');


	console.log("Signing Token!");
	let token = {
		_id: user._id.toHexString(),
		session_name: user.sessions[user.sessions.length - 1],
	} ;

	await user.save();
	console.log("User saved!");
	console.log('--------------------------------------------------');

	console.log("Checking database!");
	console.log(await database.users.findOne({name: user.name}));

	console.log('--------------------------------------------------');

	console.log("Encoding Token!");
	console.log(SESSION_TIMEOUT);
	let encoded_token = await jwt.sign(token, TOKEN_SECRET, {expiresIn: SESSION_TIMEOUT });
	console.log("Token Encoded!");

	return res . status ( 201 ) . json ( {
		token :  encoded_token,
		message : "Session Created!",
	} ) ;
} ;
