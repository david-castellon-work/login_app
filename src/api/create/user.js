import database from 'database';

export default async ( req , res , next ) => {
	console.log("/create/user");
	let message = '';
	let status_number = 200;

	if ( (!req.body.name) || (!req.body.password) ) {
		return res . status ( 200 ) . json ( { message : 'No username or no password!' } ) ;
	}

	let user = false;
	try {
		user = await database.users.create(req.body);
		if ((!user)) {
			message = 'Failed User Creation!';
		} else {
			message = 'User Created!';
			status_number = 201;
		}
	} catch (error) {
		message = 'Failed User Creation. MongoDB Error Code: ' + error.code;
		status_number = 200;
		if (error.code == 11000) message = 'User Already Exists!';
	}

//	console.log(message);

	return res . status ( status_number ) . json ( { message : message } ) ;
} ;
