import database from 'database';

export default async ( req , res , next ) => {
	console.log("/get/all_user");
	let message = '';
	let status_number = 200;

	let users = false;
	try {
		users = await database.users.find();
		message = 'Success!';
	} catch (error) {
		message = 'Failed!';
		status_number = 400;
	}

//	console.log(message);

	return res . status ( status_number ) . json ( { message : message } ) ;
} ;
