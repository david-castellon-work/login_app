import path from 'path';
import express from 'express';


// API Routes:
const router = express . Router ( ) ;


router . use ( ( req , res , next ) => {
	console.log('--------------------------------------------------');
	console.log("req.body");
	console.log(req.body);
	console.log('--------------------------------------------------');
	return next();
} ) ;

const protected_require = path => [ require('./authenticated/').default , require('./authenticated/' + path) ] ;
const doubly_protected_require = path => [ require('./authenticated/doubly'), require('./authenticated/').default , require('./authenticated/doubly/' + path) ] ;

const authenticated_post = path => {
	router . post (
		'/authenticated/' + path ,
		protected_require( path )
	) ;
} ;

const authenticated_doubly_post = path => {
	router . post (
		'/authenticated/doubly/' + path ,
		doubly_protected_require( path )
	) ;
} ;

//////////////////////////////////////////////////
// Server Routes
//////////////////////////////////////////////////

router . get ( '/ping', async ( req , res ) => res . status ( 200 ) . json ( { message : 'ping' } ) ) ;

router . post ( '/create/user'    , require('./create/user') ) ;
router . post ( '/create/session' , require('./create/session') ) ;
// router . post ( '/get/all_users'  , require('./get/user') );

authenticated_post ( 'logout' ) ;
authenticated_post ( 'user/name_get' ) ;
authenticated_post ( 'user/phone_get' ) ;
authenticated_post ( 'user/bio_get' ) ;
authenticated_post ( 'user/email_get' ) ;
authenticated_post ( 'user/pic_get' ) ;

authenticated_doubly_post ( 'user/name_change'     ) ;
authenticated_doubly_post ( 'user/bio_change'      ) ;
authenticated_doubly_post ( 'user/password_change' ) ;
authenticated_doubly_post ( 'user/email_change'    ) ;
authenticated_doubly_post ( 'user/phone_change'    ) ;
authenticated_doubly_post ( 'user/pic_change'      ) ;

authenticated_doubly_post ( 'user/expunge'         ) ;

export default router ;
