import database from 'database';

export default async (req, res, next) => {
	console.log("--------------------------------------------------");
	console.log("Double Authentication checking user credentials!");
	let user;

	if (req.body.name) {
		user = await database.users.findOne({ name: req.body.name }).exec();
		console.log("req.body has name");
	} else if (req.body.email) {
		user = await database.users.findOne({ email: req.body.email }).exec();
		console.log("req.body has email");
	} else if (req.body.phone) {
		user = await database.users.findOne({ phone: req.body.phone }).exec();
		console.log("req.body has phone");
	} else {
		console.log("req body has no name, email, or phone");
		return res . status ( 200 ) . json ( { message : "req.body has no property name, email, or phone!" } ) ;
	}

	if (!user) {
		return res . status ( 200 ) . json ( { message : "User does not exist!" } ) ;
	}

	console.log("user");
	console.log(user);

	console.log('--------------------------------------------------');

	console.log("Checking Password!");
	if ( !req.body.password ) {
		return res . status ( 200 ) . json ( { message : "No user password!" } ) ;
	}

	const is_password = await user.is_password(req.body.password) ;
	console.log(`is_password: ${is_password}`);
	if ( !is_password ) {
		return res . status ( 200 ) . json ( { message : "Password Mismatch!" } ) ;
	}
	console.log("Passwords Matched!");
	console.log('--------------------------------------------------');

	console.log(user);

	console.log('--------------------------------------------------');
	return next();
} ;
