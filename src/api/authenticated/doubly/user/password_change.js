import database from 'database';

export default async (data, req, res, next) => {
	console.log("/authenticated/doubly/user/password_change");

	await data.user.change_password(req.body.new_password);
	await data.user.save();

	return res . status ( 201 ) . json ( { message : 'User password changed!' } ) ;
};
