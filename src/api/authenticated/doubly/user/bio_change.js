export default async (data, req, res, next) => {
	console.log("/authenticated/doubly/user/bio_change");

	if (!req.body.new_bio)
		return res.status(200).json({ message: "No Bio!" });

	data.user.bio = req.body.new_bio;
	await data.user.save();

	res.status(201).json({ message: 'Bio updated!' });
};
