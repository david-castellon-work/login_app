export default async (data, req, res, next) => {
	console.log("/authenticated/doubly/user/phone_change");
	if (!req.body.new_phone)
		return res.status(200).json({ message: "No Phone!" });

	data.user.phone = req.body.new_phone;
	await data.user.save();

	res.status(201).json({ message: 'Phone updated!' });
};
