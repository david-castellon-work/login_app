export default async (data, req, res, next) => {
	console.log("/authenticated/doubly/user/pic_change");

	// How is the new pic stored in the request body?
	if (!req.files)
		return res.status(200).json({ message: "No File Uploaded!" });

	if (!req.files.pic)
		return res.status(200).json({ message: "No Pic!" });

	console.log(req.files);

	let pic = req.files.pic ;
	

//	data.user = pic;
//	pic.mv('./uploads' + pic.name);
	data.user.pic = pic.data;
	await data.user.save();

	res.status(201).json({ message: 'Pic updated!' });
};
