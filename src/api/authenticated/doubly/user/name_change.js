export default async (data, req, res, next) => {
	console.log("/authenticated/doubly/user/name_change");

	if (!req.body.new_name)
		return res.status(200).json({ message: "No New Username!" });

	data.user.name = req.body.new_name;
	await data.user.save();

	res.status(201).json({ message: 'User name updated!' });
};
