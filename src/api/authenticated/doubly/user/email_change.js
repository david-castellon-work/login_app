export default async (data, req, res, next) => {
	console.log("/authenticated/doubly/user/email_change");

	if (!req.body.new_email)
		return res.status(200).json({ message: "No Email!" });

	data.user.email = req.body.new_email;
	await data.user.save();

	res.status(201).json({ message: 'Email updated!' });
};
