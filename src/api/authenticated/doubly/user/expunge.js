import req_has_user_and_password from 'lib/req_has_user_and_password.js';
// Only used for debugging!
import database from 'database';

export default async (data, req, res, next) => {
	console.log("/authenticated/doubly/user/expunge");

	data.user.remove();

	return res . status ( 201 ) . json ( { message : 'User Deleted!' } ) ;
}
