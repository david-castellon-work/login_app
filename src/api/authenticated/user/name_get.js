export default async (data, req, res, next) => {
	console.log("/authenticated/doubly/user/name_get");

	if (!data.user.name)
		return res.status(200).json({ message: "No Name!" });

	return res.status(201).json({ name: data.user.name });
};
