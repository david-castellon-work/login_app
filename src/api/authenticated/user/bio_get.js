export default async (data, req, res, next) => {
	console.log("/authenticated/doubly/user/bio_get");

	if (!data.user.bio)
		return res.status(200).json({ message: "No Bio!" });

	return res.status(201).json({ bio: data.user.bio });
};
