export default async (data, req, res, next) => {
	console.log("/authenticated/doubly/user/pic_get");

	if (!data.user.pic)
		return res.status(200).json({ message: "No Pic!" });

	return res.status(200).json({ pic: data.user.pic });
};
