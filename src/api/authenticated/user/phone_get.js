export default async (data, req, res, next) => {
	console.log("/authenticated/doubly/user/phone_get");

	if (!data.user.phone)
		return res.status(200).json({ message: "No Phone!" });

	return res.status(201).json({ phone: data.user.phone });
};
