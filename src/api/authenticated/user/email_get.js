export default async (data, req, res, next) => {
	console.log("/authenticated/doubly/user/email_get");

	if (!data.user.email)
		return res.status(200).json({ message: "No Email!" });

	return res.status(201).json({ email: data.user.email });
};
