import database from 'database';

export default async (data, req, res, next) => {
	// Connects to socketio server if session is valid.
	console.log('/authenticated/connect');

	let socket_id = [];
	const io = req.app.get('socketio');


	io.on('connection', socket => {
		socket_id.push(socket.id);
		if (socket_id[0] === socket.id) {
			io.removeAllListeners('connection');
		}

		socket.on('hello message', msg => {
			console.log('just got: ', msg);
			socket.emit('chat message', 'hi from server');
		});
	});

	data.user.remove_session(data.session_name);
	console.log("data user alteration:");
	console.log(data.user);
	await data.user.save();
	console.log("Data user alteration saved to database!");
	console.log("Database check!");
	console.log(await database.users.findOne({username: data.user.username}));

	res . status ( 201 ) . json ( { message : 'User Session Token Deleted!' } ) ;
	return;
}
