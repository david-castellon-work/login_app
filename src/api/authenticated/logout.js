import database from 'database';

export default async (data, req, res, next) => {
	console.log('/authenticated/logout');

	data.user.remove_session(data.session_name);
	console.log("data user alteration:");
	console.log(data.user);
	await data.user.save();
	console.log("Data user alteration saved to database!");
	console.log("Database check!");
	console.log(await database.users.findOne({username: data.user.username}));

	res . status ( 201 ) . json ( { message : 'User Session Token Deleted!' } ) ;
	return;
}
