import { TOKEN_SECRET } from 'root/env.json' ;
import jwt from 'jsonwebtoken' ;
import database from 'database' ;

export const socket_authenticated = async (socket, next) => {
	console.log ( "Attempting to Authenticate a Socket IO Connection!" ) ;
	// Connection is refused.
	// Client Event is connect_error.
	console.log("Checking handshake!");
	if ( !socket.handshake            ) return next(new Error("No Handshake!"));
	console.log("Checking handshake.auth!");
	if ( !socket.handshake.auth       ) return next(new Error("No Authentication!"));
	console.log("Checking handshake.auth.token!");
	if ( !socket.handshake.auth.token ) return next(new Error("No Authentication Token!"));
	console.log("handshake.auth.token exists!");

	// See token definition in ../create/session.js
	let token = await jwt.verify(socket.handshake.auth, TOKEN_SECRET);

	console.log ( "Verified Token!" );

	if ( !token ) return next(new Error("Invalid Token!"));
	console.log ( "Valid Token!" );
	console.log ( token );

	let user = await database.users.findOne({_id: token._id});

	console.log ( "User found!" );

	if ( !user ) return next(new Error("User Not Found!"));

	console.log("token");
	console.log(token);

	if ( !user.in_session(token.session_name) ) return next(new Error("Session Not Found!"));

	console.log("Valid Session!");

	console.log("Successful Login!");

	// Next function should be able to manipulate the
	// data object or the user object however they want
	// to.
	return next({ user: user, session_name: token.session_name}); // , token.session_name);

} ;

// Should decode the token.
export default async function authenticate(req, res, next) {
	console.log("/authenticated or /doubly_authenticated");
	console.log("Checking Session Token!");

	// Return if no token.
	if (!req.headers['authorization'])
		return res.status(200).json( { message: "No Session Token!" } ) ;

	console.log("Got token!");

	// See token definition in ../create/session.js
	let token = await jwt.verify(req.headers['authorization'], TOKEN_SECRET);

	console.log ( "Verified Token!" );

	if (!token)
		return res.status(200).json( { message: "Invalid Token!" } ) ;

	console.log ( "Valid Token!" );
	console.log ( token );

	let user = await database.users.findOne({_id: token._id});

	console.log ( "User found!" );

	if (!user)
		return res.status(200).json( { message: "User Not Found!" } );

	console.log("token");
	console.log(token);

	if ( !user.in_session(token.session_name) )
		return res.status(200).json( { message: "Session Not Found!" } );

	console.log("Valid Session!");

	console.log("Successful Login!");

	// Next function should be able to manipulate the
	// data object or the user object however they want
	// to.
	return next({ user: user, session_name: token.session_name}); // , token.session_name);
}
