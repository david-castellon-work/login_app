import database from 'database' ;
import { socket_authenticated } from './authenticated';

export default socket_io_server => {
	socket_io_server.use(socket_authenticated);

	socket_io_server.on('connection' , socket => {
		console.log( 'User successfully connected!' ) ;

		// These are notifications for everyone.
		// Does the middleware pass into the parameter
		socket.on('new request posting', ( data ) => {
			console.log(data);
			// let { user , session_name } = data;
			// // socket.id for 
			// // socket.to(/* another socket id */).emit("hey");


			// console.log("New request posting!");
			// console.log(data);
			// console.log("New Request Posting!");
			// console.log(data);
			// // Request Posting Needs to be saved to database.
			// socket.broadcast.emit('new request posting', socket.id);
		} ) ;

		socket.on('received message', data => {
		} ) ;

		socket.on('sent message', data => {
		} ) ;

		socket.on('disconnect', () => {
			console.log('User Disconnected!');
		} ) ;
	} ) ;
} ;
