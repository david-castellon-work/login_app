import http_server          from 'root/app';
import { socket_io_server } from 'root/app';
import database             from 'root/database';

export default async () => {
	socket_io_server.close();
	http_server.close();
} ;
