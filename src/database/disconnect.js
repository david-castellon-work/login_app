import mongoose from 'mongoose';

export default async () => {
	return mongoose.connection.close();
} ;
