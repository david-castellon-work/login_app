import mongoose from 'mongoose';

export default async ( ) => {
	if (!mongoose.connection.db) throw new Error("No Database!");
	await mongoose.connection.db.dropDatabase();
	// return mongoose.connection.close();
} ;
