import ENV from 'root/env.json';
import mongoose from 'mongoose';
import user from './models/user' ;

export default async ( ) => {
	try {
		console . log ( '--------------------------------------------------' ) ;
		console . log ( 'Connecting to DataBase!' ) ;
		console . log ( `ENV.DB_URI = ${ENV.DB_URI}` ) ;
		return mongoose . connect (
			ENV.DB_URI ,
			{
				useNewUrlParser : true		,
				useCreateIndex : true		,
				useUnifiedTopology : true	,
			}
		) ;
		user . init () ;
		console . log ( 'Database connected!' ) ;
		mongoose . connection . on ( 'error' , err => {
			throw err ;
		} ) ;
		console . log ( '--------------------------------------------------' ) ;
	} catch ( err ) {
		throw `MongoDB initial connection error: ${err}`
	}
}
