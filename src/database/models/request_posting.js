import mongoose         from 'mongoose'		;

const SALT_SHAKES_INTEGER = parseInt ( process . env . SALT_SHAKES ) ;

var request_posting = new mongoose . Schema ( {
	owner: {
		type: mongoose.Schema.ObjectId,
		ref: 'users',
	},
	pic: Buffer,
	desc: String,
	message_thread: {
		type: mongoose.Schema.ObjectId,
		ref: 'messages',
	} ,
} ) ;

export default mongoose . model ( 'request_posting' , request_posting , 'request_postings' );
