import jwt              from 'jsonwebtoken'     ;
import mongoose         from 'mongoose'		;
import bcrypt           from 'bcrypt'		;
// import Validator        from 'validator'	;

import {
	SALT_SHAKES ,
	TOKEN_SECRET ,
	SESSION_TIMEOUT
} from 'root/env.json' ;

const SALT_SHAKES_INTEGER = parseInt ( process . env . SALT_SHAKES ) ;

var user_schema = new mongoose . Schema ( {
	pic: Buffer,
	bio: String,
	name:
	{
		type: String,
		unique: true,
		required: true,
		dropDups: true,
	},
	email:
	{
		type: String,
		// unique: true,
		// required: true,
		dropDups: true,
	},
	phone:
	{
		type: String,
		// unique: true,
		dropDups: true,
	},

	password_hash: {
		type: String,
		required: true,
	},

	sessions: [ {
		type: String,
		// unique: true,
		createdAt: {
			type: Date, 
			expires: SESSION_TIMEOUT,
			default: Date.now,
		}
	} ] ,
	request_postings: [ {
		type: mongoose.Schema.ObjectId,
		ref: 'request_postings',
	} ] ,
} ) ;

// Password Methods:
user_schema . virtual ( 'password' )
	. get ( function ( ) { this . _password ; } )
	. set ( function ( password ) {
//		console.log("password setter!");
//		console.log(password);

		const user = this ;

		user . _password = password ;
		// Has to be synchronous utilized in code format is as follows.
		// user.password = "password"
		user . password_hash = bcrypt . hashSync ( password , SALT_SHAKES_INTEGER ) ;

//		console.log('--------------------------------------------------');

	} ) ;

// Returns a promise.
user_schema . methods . is_password = function ( password_attempt ) {
//	console.log("is_password");
//	console.log(this.password_hash);
//	console.log('--------------------------------------------------');
	return bcrypt . compare ( password_attempt , this.password_hash ) ;
} ;

// Is in array?
user_schema . methods . in_session = function ( session_name ) {
	this.sessions_set = new Set(this.sessions) ;
	return this.sessions_set.has(session_name) ;
} ;

// change password
user_schema . methods . change_password = function ( new_password ) {
	this.password = new_password;
} ;

// Removes a session by index.
user_schema . methods . remove_session = function(session_name) {
//	console.log(`Removing Session: ${session_name}`);
//	console.log('--------------------------------------------------');

	// Sessions_set should already be a property.
	if ( !this.sessions_set ) this.sessions_set = new Set(this.sessions) ;

	this.sessions_set.delete(session_name);

	this . sessions = Array . from ( this.sessions_set ) ;

} ;


// // Pushes a session.
user_schema . methods . push_session = function(session_name_parameter) {
	let session_name;

	let sessions_set = new Set(this.sessions);

	if ( session_name_parameter ) {
		session_name = session_name_parameter ;
	} else {
		session_name = `Session ${sessions_set.size}` ;
	}

	sessions_set.add(session_name);

//	console.log(sessions_set);
//	console.log(this);

	this . sessions = Array . from ( sessions_set ) ;

//	console.log(this);
} ;

user_schema . pre ( 'create' , next => {
//	console.log("pre create");
} ) ;

user_schema . post ( 'create' , next => {
//	console.log("post create");
} ) ;
// user_schema . statics . create_user = async user => {
// 	
// } ;

export default mongoose . model ( 'user' , user_schema , 'users' );
