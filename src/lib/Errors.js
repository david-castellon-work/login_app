export default {
	USR_V		: 'User is verified!' ,
	USR_ENV		: 'User exists but not verified!' ,
	USR_ENE		: 'User exists but not verified and no email!' ,
	USR_DNE		: 'User does not exist!' ,
	PASS_DNM	: 'User passwords do not match!' ,
	SER_ER		: 'Server Error: ' ,
	NTM		: 'No token matches!' ,
} ;
