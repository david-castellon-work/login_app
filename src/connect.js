import database from 'root/database';
// import http_server from 'root/app';

import ENV from 'root/env.json';

export default async function connect(http_server) {

	console.log("Connection!");
	try {
		await database.connect();
		http_server.listen(ENV.PORT, console.log(`User Login On: localhost:${ENV.PORT}`));
	} catch ( err ) {
		console.error(err);
	}
}
