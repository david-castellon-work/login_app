import create_user from './user';
import database from 'root/database';
import { TEST_USER_USERNAME , TEST_USER_PASSWORD } from 'root/env.json';


export default ( request , expect , _sessions , _username , _password ) => async () => {
	let username = TEST_USER_USERNAME;
	let password = TEST_USER_PASSWORD;
	let sessions = 1;

	if ( _sessions ) sessions = _sessions ;
	if ( _username ) username = _username ;
	if ( _password ) password = _password ;

	const create_user_response = await create_user(request, expect, username, password)();

	let responses = [ ] ;

	for ( let i = 0 ; i < sessions ; i++ ) {
		responses . push ( await request
			.post("/create/session")
			.send( {
				name: username ,
				password: password
			} ) ) ;
	}

	let user = await database.users.findOne({ name: TEST_USER_USERNAME }).exec() ;

	responses.forEach( async ( response , index ) => {
		expect(response.body).toHaveProperty('message');
		expect(response.body).toHaveProperty('token');
		expect(response.statusCode).toBe(201);
		expect(user.sessions).toHaveProperty(`${index}`);
	} ) ;

	// Returns an array containing just tokens.
	// return responses.map( async response => response.body.token ) ;

	return responses ;

}; 
