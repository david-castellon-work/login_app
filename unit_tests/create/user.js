import database from 'root/database';
import { TEST_USER_USERNAME , TEST_USER_PASSWORD } from 'root/env.json';

export default ( request , expect , _username , _password ) => async () => {

	let username = TEST_USER_USERNAME;
	let password = TEST_USER_PASSWORD;

	if ( _username ) username = _username ;
	if ( _password ) password = _password ;

	const response = await request
		.post("/create/user")
		.send( {
			name: TEST_USER_USERNAME,
			password: TEST_USER_PASSWORD
		} );

	expect(response.statusCode).toBe(201);
	expect(response.body).toHaveProperty('message');

	let user = await database.users.findOne({ name: TEST_USER_USERNAME }).exec() ;
	console.log(`database query for user with name: ${TEST_USER_USERNAME}`);
	console.log(user);
	expect(user.name).toBe(TEST_USER_USERNAME);

	return response;
} ;
