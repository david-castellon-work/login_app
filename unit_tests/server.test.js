import supertest from 'supertest';

import database from 'root/database';
import { http_server , socket_io_server } from 'root/app';

import { PORT } from 'root/env.json';

// http server on 
var request = supertest(http_server.listen(PORT));

function test_route(path) {
	let unit_test = require("." + path);
	return it(path, unit_test(request, expect, socket_io_server));
} ;


var http_server_addr;

beforeAll(async done => {
	await database.connect();

	done();
} ) ;
afterAll(async done => {
	socket_io_server.close();
	http_server.close();
	await database.disconnect();
	done();
} );

beforeEach(async done => {
	// Connect database, http server, and socketio server.
	// await connect();
	jest.setTimeout(20000); // 20 seconds
	done()
} ) ;

afterEach( async done => {
	try {
		await database.delete_everything();
	} catch ( err ) {
		// if ( err.message == "No Database!" ) 
		console.log(err.message);
	} finally {
		done();
	}
} ) ;


// test_route("/ping"            ) ;
// 
// test_route("/create/user"     ) ;
// test_route("/create/session"  ) ;
// test_route("/authenticated/logout" ) ;
// test_route("/authenticated/user/name_get") ;
// test_route("/authenticated/user/email_get") ;
// test_route("/authenticated/user/phone_get") ;
// test_route("/authenticated/user/bio_get") ;
// // test_route("/authenticated/user/pic_get") ;
// 
// test_route("/authenticated/doubly/user/expunge" ) ;
// test_route("/authenticated/doubly/user/name_change" ) ;
// test_route("/authenticated/doubly/user/email_change" ) ;
// test_route("/authenticated/doubly/user/phone_change" ) ;
// test_route("/authenticated/doubly/user/password_change" ) ;
// test_route("/authenticated/doubly/user/bio_change" ) ;
// test_route("/authenticated/doubly/user/pic_change" ) ;

test_route("/events/new_request_posting");
