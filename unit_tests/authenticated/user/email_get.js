import database from 'root/database';

import email_change from "root/unit_tests/authenticated/doubly/user/email_change";
import { TEST_USER_USERNAME , TEST_USER_PASSWORD } from 'root/env.json';

export default ( request , expect ) => async () => {

	const token = await email_change(request, expect)();

	console.log("Create Session Response!");
	console.log(token); // token

	const response = await request
		.post("/authenticated/user/email_get")
		.set("authorization", token)
		.send( ) ;

	console.log("Create Response Body!");
	console.log(response.body);

	expect(response.body).toHaveProperty('email');
	expect(response.statusCode).toEqual(201);
} ;
