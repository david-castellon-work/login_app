import database from 'root/database';

import bio_change from "root/unit_tests/authenticated/doubly/user/bio_change";
import { TEST_USER_USERNAME , TEST_USER_PASSWORD } from 'root/env.json';

export default ( request , expect ) => async () => {

	const token = (await bio_change(request, expect)());

	console.log("Create Session Response!");
	console.log(token); // token

	const response = await request
		.post("/authenticated/user/bio_get")
		.set("authorization", token)
		.send( ) ;

	console.log("Create Response Body!");
	console.log(response.body);

	expect(response.body).toHaveProperty('bio');
	expect(response.statusCode).toEqual(201);
} ;
