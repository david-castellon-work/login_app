import database from 'root/database';
import create_session from "root/unit_tests/create/session";
import { TEST_USER_USERNAME , TEST_USER_PASSWORD } from 'root/env.json';

export default ( request , expect ) => async () => {

	const create_session_response = await create_session(request, expect, 1)(); // [0].body.token;
	const token = create_session_response[0].body.token;

	console.log("Create Session Response!");
	console.log(token); // token

	const response = await request
		.post("/authenticated/logout")
		.set("authorization", token)
		.send( {
			name: TEST_USER_USERNAME,
			password: TEST_USER_PASSWORD
		} ) ;

	expect(response.body).toHaveProperty('message');
	expect(response.statusCode).toEqual(201);

	let user = await database.users.findOne({ name: TEST_USER_USERNAME }).exec() ;
	expect(user.sessions.length).toEqual(0);
} ;
