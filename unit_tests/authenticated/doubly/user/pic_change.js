import path from 'path';
import database from 'root/database';
import create_session_func from "root/unit_tests/create/session";
import { TEST_USER_USERNAME , TEST_USER_PASSWORD } from 'root/env.json';

export default ( request , expect ) => async () => {

	const create_session_response = await create_session_func(request, expect)();
	const token = create_session_response[0].body.token;

	console.log("Create Session Response!");
	console.log(token); // token

//	const new_pic = 'keeper@gmail.com';
	const file_path = path . resolve ( __dirname , './blue-block.png' ) ;
	const response = await request
		.post("/authenticated/doubly/user/pic_change")
		.set("authorization", token)
		.field("name", TEST_USER_USERNAME)
		.field("password", TEST_USER_PASSWORD)
		.attach('pic', file_path);

	console.log("Create Response Body!");
	console.log(response.body);

	expect(response.body).toHaveProperty('message');
	expect(response.statusCode).toEqual(201);

	let user = await database.users.findOne({name: TEST_USER_USERNAME});

//	expect(user.pic).toBe(new_pic);
	return token
} ;
