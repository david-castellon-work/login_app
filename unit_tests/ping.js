export default ( request , expect ) => async () => {
	console.log('ping test')
	const response = await request.get("/ping");

	console.log('received response')
	expect(response.statusCode).toBe(200);
	console.log('checked status code response')
	console.log("checked message");
	expect(response.body.message).toBe('ping');
} ;
