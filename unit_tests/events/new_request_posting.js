import database from 'root/database';
import create_session from 'root/unit_tests/create/session';
import socketio_client from 'socket.io-client.js';

import { SERVER_HOST , PORT , TEST_USER_USERNAME , TEST_USER_PASSWORD } from 'root/env.json';

export default ( request , expect ) => async () => {
// async function new_request_posting ( ) {

	const create_session_response = await create_session(request, expect, 1)(); // [0].body.token;
	const token = create_session_response[0].body.token;

	// Tries to connect with the session token.
	const socket = socketio_client.connect(`http://${SERVER_HOST}:${PORT}`, {
		auth: token,
	} );

	socket.on("connect_error", err => {
		console.log("Socket not able to connect!");
		console.log("Error!");
		console.log(err);
	} ) ;

	// Store socket identifier in database.
	socket.on("connect", socket => {
		console.log("Socket successfully connected!");

		socket.on("new request posting", (data, callback) => {
			console.log("Received New Request Posting!");
			console.log(data);
		} ) ;

	} ) ;

//	expect(response.statusCode).toBe(200);
//	return expect(response.body.message).toBe('ping');
//
//	// Check!
//	expect(response.body).toHaveProperty('message');
//	expect(response.statusCode).toEqual(201);
//
//	let user = await database.users.findOne({ name: TEST_USER_USERNAME }).exec() ;
//	expect(user.sessions.length).toEqual(0);

// 	return;
} ;

// console.log(socket.emit('new request posting', data));
