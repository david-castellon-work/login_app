# Login

* Create User
* Verify User
* Login User
* Logout User

## Scripts

	// Transpiles all source code.
	npm run build
	// Starts the server.
	npm run start
	// Autorestarts server on source code change.
	npm run watch-server
	// Runs unit tests in watch mode
	npm run watch-unit_test

## Configuration

1. Install mongodb
2. Start mongodb with default settings.
3. Create user account.
	use admin
	db.createUser(
		{
			user: "login_app",
			pwd: "password",
			roles: [ { role: "userAdminAnyDatabase", db: "admin" }, "readWriteAnyDatabase" ]
		}
	);


## Todo

1. Global feed.
2. Messenging
